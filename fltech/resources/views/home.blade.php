<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<!-- META -->
		<meta charset="UTF-8">
		<meta name="description" content="Fl-technics, Linas Janulevičius">
		<meta name="author" content="Linas Janulevičius">
		<meta name="keywords" content="Janulevicius, Linas, fl-technics">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2.0, user-scalable=yes">
		<meta name="robots" content="index, follow">
		<meta name="robots" content="noimageindex">
		<meta name="robots" content="noarchive">
		<meta name="google" content="notranslate">
		<!-- LINK -->
		<link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
		<link href="{{ asset('css/style.css') }}" rel="stylesheet">
		<!-- TITLE -->
		<title>App design Home</title>
	</head>
	<body>
		<!-- Semantiniai elementai, kurie sumažina arba visai pašalina klasių ir id naudojimą -->
		<main>
			<script>
			function func() {
				document.getElementById("menu").classList.add("navigation");
			}
			</script>
			<nav id="menu">
				<figure>
					<img src="{{ asset('images/avatar.jpg') }}" alt="avatar">
					<figcaption>Andrea Pierce <address>andre@invisionapp.com</address></figcaption>
				</figure>
				<a href="{{ url('home') }}">Home</a>
				<a href="{{ url('addnew') }}">Add New</a>
				<a href="{{ url('signin') }}">Logout</a>
			</nav>
			<header>
				<button onclick="func()">&#x2630;</button>
				<h1>Home</h1>
				<a href="{{ url('addnew') }}">&#43;</a>
			</header>
			<h2>TUESDAY, MARCH 9</h2>
			<dl>
				<dt>Meeting with Janet</dt>
				<dd>8 — 10 am &#x26B2; Starbucks</dd>
			</dl>
			<dl>
				<dt>Meeting with Janet</dt>
				<dd>8 — 10 am &#x26B2; Starbucks</dd>
			</dl>
			<dl>
				<dt>Meeting with Janet</dt>
				<dd>8 — 10 am &#x26B2; Starbucks</dd>
			</dl>
			<h2>TUESDAY, MARCH 9</h2>
			<dl>
				<dt>Meeting with Janet</dt>
				<dd>8 — 10 am &#x26B2; Starbucks</dd>
			</dl>
			<dl>
				<dt>Meeting with Janet</dt>
				<dd>8 — 10 am &#x26B2; Starbucks</dd>
			</dl>
			<small></small>
		</main>
	</body>
</html>