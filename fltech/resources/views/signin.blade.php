<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<!-- META -->
		<meta charset="UTF-8">
		<meta name="description" content="Fl-technics, Linas Janulevičius">
		<meta name="author" content="Linas Janulevičius">
		<meta name="keywords" content="Janulevicius, Linas, fl-technics">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2.0, user-scalable=yes">
		<meta name="robots" content="index, follow">
		<meta name="robots" content="noimageindex">
		<meta name="robots" content="noarchive">
		<meta name="google" content="notranslate">
		<!-- LINK -->
		<link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
		<link href="{{ asset('css/style.css') }}" rel="stylesheet">
		<!-- TITLE -->
		<title>App design Sign In</title>
	</head>
	<body>
		<!-- Semantiniai elementai, kurie sumažina arba visai pašalina klasių ir id naudojimą -->
		<main>
			<img id="logo" src="{{ asset('images/logo.svg') }}" alt="logo">
			<footer>
				<!-- GET tik demo tikslais, nes visada tokio tipo formai bus taikomas POST metodas -->
				<form action="/home" method="get">
					<label>USERNAME<input type="email" name="username" maxlength="30"></label>
					<label>PASSWORD<span><input type="password" name="password" minlength="8" required>
						<a href="javascript:">Forgot Password</a></span>
					</label>
					<input type="submit" value="Sign In">
				</form>
				<small>DON'T HAVE AN ACCOUNT? <a href="{{ url('signup') }}">SIGN UP</a></small>
			</footer>
		</main>
	</body>
</html>
