<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<!-- META -->
		<meta charset="UTF-8">
		<meta name="description" content="Fl-technics, Linas Janulevičius">
		<meta name="author" content="Linas Janulevičius">
		<meta name="keywords" content="Janulevicius, Linas, fl-technics">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2.0, user-scalable=yes">
		<meta name="robots" content="index, follow">
		<meta name="robots" content="noimageindex">
		<meta name="robots" content="noarchive">
		<meta name="google" content="notranslate">
		<!-- LINK -->
		<link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
		<link href="{{ asset('css/style.css') }}" rel="stylesheet">
		<!-- TITLE -->
		<title>App design Sign Up</title>
	</head>
	<body>
		<!-- Semantiniai elementai, kurie sumažina arba visai pašalina klasių ir id naudojimą -->
		<main>
			<header>
				<a href="{{ url('signin') }}">&#9587;</a>
				<h1>Sign up</h1>
			</header>
			<figure>
				<img src="{{ asset('images/avatar.jpg') }}" alt="avatar">
				<button type="button">+</button>
			</figure>
			<footer>
				<form action="/signin" method="get">
					<label>FULL NAME<input type="text" name="fullname" maxlength="50"></label>
					<label>EMAIL<input type="email" name="username" maxlength="30"></label>
					<label>PASSWORD<input type="password" name="password" minlength="8" required></label>
					<label>BIRTHDAY<input type="date" name="calendar"></label>
					<input type="submit" value="Sign In">
				</form>
				<small>ALREADY HAVE AN ACCOUNT? <a href="{{ url('signin') }}">SIGN IN</a></small>
			</footer>
		</main>
	</body>
</html>