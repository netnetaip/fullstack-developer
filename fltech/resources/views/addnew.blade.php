<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<!-- META -->
		<meta charset="UTF-8">
		<meta name="description" content="Fl-technics, Linas Janulevičius">
		<meta name="author" content="Linas Janulevičius">
		<meta name="keywords" content="Janulevicius, Linas, fl-technics">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2.0, user-scalable=yes">
		<meta name="robots" content="index, follow">
		<meta name="robots" content="noimageindex">
		<meta name="robots" content="noarchive">
		<meta name="google" content="notranslate">
		<!-- LINK -->
		<link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
		<link href="{{ asset('css/style.css') }}" rel="stylesheet">
		<!-- TITLE -->
		<title>App design Add New</title>
	</head>
	<body>
		<!-- Semantiniai elementai, kurie sumažina arba visai pašalina klasių ir id naudojimą -->
		<main>
			<header>
				<a href="{{ url('home') }}">&#x2715;</a>
				<h1>Add New</h1>
				<a href="{{ url('home') }}">&#10004;</a>
			</header>
			<footer>
				<form action="/home" method="get">
					<label id="title">TITLE<input type="text" name="title" maxlength="50"></label>
					<label id="description" >DESCRIPTION<input type="text" name="description" maxlength="30"></label>
					<label>DATE<input type="date" name="date"></label>
					<label class="time">FROM<input type="time" name="from" min="0:00" max="23:00" required></label>
					<label class="time">TO<input type="time" name="to" min="0:00" max="23:00" required></label>
					<label>LOCATION:<span><input type="text"><a href="javascript:">&#x26B2;</a></span></label>
					<label>NOTIFY:<span><input type="time"><a href="javascript:">&#x2193; Email</a></span></label>
					<label>LABEL:
						<span>
							Colour
							<select>
								<option value="1" style="background-color: blue">Blue</option>
								<option value="2" style="background-color: teal">Teal</option>
							</select>
						</span>
					</label>
				</form>
			</footer>
			<small></small>
		</main>
	</body>
</html>