<?php

Route::get('/', function () {
    return view('signin');
});

Route::get('/signin', function () {
    return view('signin');
});

Route::get('/signup', function () {
    return view('signup');
});

Route::get('/addnew', function () {
    return view('addnew');
});

Route::get('/home', function () {
    return view('home');
});