<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    public function up()
{
    Schema::create('tasks', function (Blueprint $table) {
	  $table->increments('id');
	  $table->string('description');
	  $table->integer('user_id')->unsigned()->index();
	  $table->timestamps();
    });
}
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
